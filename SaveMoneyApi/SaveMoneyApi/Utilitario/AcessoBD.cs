﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace SaveMoneyApi.Utilitario
{
    public class AcessoBD
    {
        private SqlConnection objConnection = new SqlConnection();
        private SqlTransaction objTransaction = null;

        public void BeginTransaction()
        {
            AbrirBanco();
            if (objTransaction == null)
                objTransaction = objConnection.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (objTransaction.Connection != null)
            {
                objTransaction.Commit();
                objTransaction.Dispose();
            }
        }

        public void RollBackTransaction()
        {
            if (objTransaction != null && objTransaction.Connection != null)
                objTransaction.Rollback();
        }

        // Abre conexão
        public void AbrirBanco()
        {

            if (objConnection.State != ConnectionState.Open)
            {

                objConnection.ConnectionString = WebConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
                objConnection.Open();

            }
        }

        // Fecha conexão
        public void FecharBanco()
        {

            if (objConnection.State == ConnectionState.Open && (objTransaction == null || (objTransaction != null && objTransaction.Connection == null)))
                objConnection.Close();
        }

        // Executa comandos de seleção
        public DataTable RetornarDataTable(SqlCommand objCmd)
        {

            DataTable oDt = new DataTable();
            try
            {

                objCmd.Connection = objConnection;
                if (objTransaction != null)
                    objCmd.Transaction = objTransaction;

                SqlDataAdapter oDa = new SqlDataAdapter(objCmd);
                oDa.Fill(oDt);

                return oDt;
            }

            catch (Exception ex)
            {
                RollBackTransaction();
                FecharBanco();
                throw ex;
            }

        }


    }
}