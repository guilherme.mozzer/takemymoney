package savemoney.savemoney.app;

import android.app.Application;

import savemoney.savemoney.service.RestService;
import savemoney.savemoney.service.ServiceUsuario;

public class SaveMoneyApplication extends Application {

    //private static final String URL = "http://192.168.1.4:6040/api/";
    private static final String URL = "http://172.16.212.136:6040/api/";
    private static SaveMoneyApplication instance;

    private ServiceUsuario serviceUsuario;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        createServices();
    }

    private void createServices(){
        serviceUsuario = (new RestService(URL).getService(ServiceUsuario.class));
    }

    public static SaveMoneyApplication getInstance() {
        return instance;
    }

    public ServiceUsuario getServiceUsuario(){
        return serviceUsuario;
    }
}
