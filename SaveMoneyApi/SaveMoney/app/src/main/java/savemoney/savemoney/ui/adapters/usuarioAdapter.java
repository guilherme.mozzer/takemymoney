package savemoney.savemoney.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import savemoney.savemoney.R;
import savemoney.savemoney.model.Usuario;

/**
 * Created by 71700609 on 20/11/2018.
 */

public class usuarioAdapter extends BaseAdapter {

    private Context context;

    private List<Usuario> listusuario;


    public usuarioAdapter(Context context, List<Usuario> listusuario) {
        this.context = context;
        this.listusuario = listusuario;
    }

    @Override
    public int getCount() {
        return listusuario.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            Usuario usuario = listusuario.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v = inflater.inflate(R.layout.activity_cadastro, null);

        TextView nome = v.findViewById(R.id.nomecadastro);
        nome.setText(usuario.getNome());

        TextView email = v.findViewById(R.id.emailcadastro);
        nome.setText(usuario.getEmail());

        TextView senha = v.findViewById(R.id.confirmsenhacadastro);
        nome.setText(usuario.getSenha());

        return v;

    }
}
