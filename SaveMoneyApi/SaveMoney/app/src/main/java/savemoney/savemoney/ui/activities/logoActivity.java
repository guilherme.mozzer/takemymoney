package savemoney.savemoney.ui.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import savemoney.savemoney.R;

public class logoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);


        //aplica um splash na tela para iniciar
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showLoginActivity();
            }
        }, 7000);

    }
    private void showLoginActivity() {
        Intent i = new Intent(this, loginActivity.class);
        startActivity(i);
        finish();
    }
}
