﻿using SaveMoneyApi.DALeADO.ADO;
using SaveMoneyApi.DALeADO.DAL;
using SaveMoneyApi.Models;
using SaveMoneyApi.Utilitario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace SaveMoneyApi.Services
{
    public class UsuarioService
    {
        private UsuarioDAL dal = new UsuarioADO();
        public Mensagem cadastrarUsuario(Usuario objUsuario)
        {
            return dal.cadastrarUsuario(objUsuario);
        }

        public Mensagem editarDadosUsuario(Usuario objUsuario)
        {
            return dal.editarDadosUsuario(objUsuario);
        }

        public Mensagem checarUsuarioExiste(string email)
        {
            return dal.checarUsuarioExiste(email);
        }

        public Mensagem logarUsuario(string email, string password)
        {
            return dal.logarUsuario(email, password);

        }
    }
}