package savemoney.savemoney.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import savemoney.savemoney.R;

/**
 * Created by 71700137 on 31/10/2018.
 */

public class addcontaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adicionarconta);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.buttonconfirma)
    public void ClickSave(){
        Intent i = new Intent(this, contaActivity.class);
        startActivity(i);

    }

    @OnClick(R.id.buttunvoltar)
    public void ClickBack(){
        Intent i = new Intent(this, contaActivity.class);
        startActivity(i);

    }
}
