package savemoney.savemoney.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import savemoney.savemoney.R;

/**
 * Created by 71700137 on 31/10/2018.
 */

public class recuperarsenhaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperarsenha);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.buttonvoltar)
    public void ClickBack(){
        Intent i = new Intent(this, loginActivity.class);
        startActivity(i);

    }

    @OnClick(R.id.buttonrecuperar)
    public void ClickRecuperar(){
        Intent i = new Intent(this, loginActivity.class);
        startActivity(i);

    }
}
