package savemoney.savemoney.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import butterknife.OnClick;
import savemoney.savemoney.R;
import savemoney.savemoney.ui.activities.contaActivity;
import savemoney.savemoney.ui.activities.despesaActivity;
import savemoney.savemoney.ui.activities.editprofileActivity;
import savemoney.savemoney.ui.activities.loginActivity;
import savemoney.savemoney.ui.activities.receitaActivity;
import savemoney.savemoney.ui.activities.signupActivity;

public class addFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add, null);
        Button addreceita = (Button) view.findViewById(R.id.addreceita);
        Button adddespesa = (Button) view.findViewById(R.id.adddespesa);
        Button addtarnferencia = (Button) view.findViewById(R.id.addtransferencia);
        addreceita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenReceita();
            }
        });

        adddespesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenDespesa();
            }
        });

        addtarnferencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenTransferencia();
            }
        });
        return view;
    }

    public void screenReceita() {
        Intent i = new Intent(getActivity(), receitaActivity.class);
        startActivity(i);
    }

    public void screenDespesa() {
        Intent i = new Intent(getActivity(), despesaActivity.class);
        startActivity(i);
    }

    public void screenTransferencia() {
        //Intent i = new Intent(getActivity(), loginActivity.class);
        //startActivity(i);
    }
}
