﻿using System;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Globalization;
using System.Text.RegularExpressions;

namespace SaveMoneyApi.Utilitario
{
    public class Funcao
    {
        private static CultureInfo cult = new CultureInfo("pt-BR");

        public static string Meio(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        public static string Esquerda(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        public static string Meio(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        public static string Direita(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        public static string trataValorReal(string str)
        {
            str = str.Equals("") ? "0" : str;
            str = decimal.Parse(str).ToString("C");
            str = str.Replace("R$ ", "");
            str = str.Replace(".", "");
            str = str.Replace(",", "");
            str = str.Replace("$", "");
            str = Esquerda(str, str.Length - 2) + "." + Direita(str, 2);

            return str;
        }

        public static String formataData(DateTime Data)
        {
            String NewData = "", Horas = "";
            if (Data.ToString() != "")
            {
                NewData = (Data.Day).ToString().PadLeft(2, '0') + (Data.Month).ToString().PadLeft(2, '0') + (Data.Year).ToString().PadLeft(2, '0');
                Horas = (Data.Hour).ToString().PadLeft(2, '0') + (Data.Minute).ToString().PadLeft(2, '0') + (Data.Second).ToString().PadLeft(2, '0');

            }
            return Horas + NewData;
        }

        public static string enCrypt(string strCryptThis)
        {

            string strEncrypted = "";
            string iCryptCharHex, iCryptCharHexStr;
            Int32 i, iKeyChar, iCryptChar;
            Int32 iStringChar;
            string g_key = Meio("dscalkwc3ro2qr3t436/$%/$%&N5cdfcadscnsijsdifjsdoifjosdijf0ui09iu09sdi09i09sidf09sidf09si", 0, strCryptThis.Length);

            for (i = 0; i < strCryptThis.Length; i++)
            {

                iKeyChar = Asc(Meio(g_key, i, 1));

                iStringChar = Asc(Meio(strCryptThis, i, 1));

                iCryptChar = iKeyChar ^ iStringChar;

                iCryptCharHex = iCryptChar.ToString("X");

                iCryptCharHexStr = iCryptCharHex;

                if (iCryptCharHexStr.Length == 1)

                    iCryptCharHexStr = "0" + iCryptCharHexStr;

                strEncrypted += iCryptCharHexStr;

            }

            return strEncrypted;

        }

        public static string deCrypt(string strCryptThis)
        {
            string strDecrypted = "";

            Int32 i, iKeyChar, iCryptChar;

            Int32 iStringChar;



            string g_key = Meio("dscalkwc3ro2qr3t436/$%/$%&N5cdfcadscnsijsdifjsdoifjosdijf0ui09iu09sdi09i09sidf09sidf09si", 0, strCryptThis.Length / 2);

            for (i = 0; i < strCryptThis.Length / 2; i++)
            {

                iKeyChar = Asc(Meio(g_key, i, 1));

                iStringChar = Convert.ToInt32(Meio(strCryptThis, (i * 2), 2), 16);

                iCryptChar = iKeyChar ^ iStringChar;

                strDecrypted = strDecrypted + (char)iCryptChar;

            }

            return strDecrypted;
        }
        
        public string tirarAcentos(string texto)
        {


            //string normalizedString = null;
            //StringBuilder stringBuilder = new StringBuilder();
            //normalizedString = texto.Normalize(NormalizationForm.FormD);
            //int i = 0;
            //char c = '\0';

            //for (i = 0; i <= normalizedString.Length - 1; i++)
            //    {
            //        c = normalizedString[i];
            //      if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
            //     {
            //        stringBuilder.Append(c);
            //     }
            //    }
            //    return stringBuilder.ToString().ToLower();

            string textor = "";

            for (int i = 0; i < texto.Length; i++)
            {
                if (texto[i].ToString() == "ã") textor += "a";
                else if (texto[i].ToString() == "á") textor += "a";
                else if (texto[i].ToString() == "à") textor += "a";
                else if (texto[i].ToString() == "â") textor += "a";
                else if (texto[i].ToString() == "ä") textor += "a";
                else if (texto[i].ToString() == "é") textor += "e";
                else if (texto[i].ToString() == "è") textor += "e";
                else if (texto[i].ToString() == "ê") textor += "e";
                else if (texto[i].ToString() == "ë") textor += "e";
                else if (texto[i].ToString() == "í") textor += "i";
                else if (texto[i].ToString() == "ì") textor += "i";
                else if (texto[i].ToString() == "ï") textor += "i";
                else if (texto[i].ToString() == "õ") textor += "o";
                else if (texto[i].ToString() == "ó") textor += "o";
                else if (texto[i].ToString() == "ò") textor += "o";
                else if (texto[i].ToString() == "ö") textor += "o";
                else if (texto[i].ToString() == "ú") textor += "u";
                else if (texto[i].ToString() == "ù") textor += "u";
                else if (texto[i].ToString() == "ü") textor += "u";
                else if (texto[i].ToString() == "ç") textor += "c";
                else if (texto[i].ToString() == "Ã") textor += "A";
                else if (texto[i].ToString() == "Á") textor += "A";
                else if (texto[i].ToString() == "À") textor += "A";
                else if (texto[i].ToString() == "Â") textor += "A";
                else if (texto[i].ToString() == "Ä") textor += "A";
                else if (texto[i].ToString() == "É") textor += "E";
                else if (texto[i].ToString() == "È") textor += "E";
                else if (texto[i].ToString() == "Ê") textor += "E";
                else if (texto[i].ToString() == "Ë") textor += "E";
                else if (texto[i].ToString() == "Í") textor += "I";
                else if (texto[i].ToString() == "Ì") textor += "I";
                else if (texto[i].ToString() == "Ï") textor += "I";
                else if (texto[i].ToString() == "Õ") textor += "O";
                else if (texto[i].ToString() == "Ó") textor += "O";
                else if (texto[i].ToString() == "Ò") textor += "O";
                else if (texto[i].ToString() == "Ö") textor += "O";
                else if (texto[i].ToString() == "Ú") textor += "U";
                else if (texto[i].ToString() == "Ù") textor += "U";
                else if (texto[i].ToString() == "Ü") textor += "U";
                else if (texto[i].ToString() == "Ç") textor += "C";
                else textor += texto[i];
            }
            return textor;
        }

        public static Int32 Asc(string ch)
        {

            //Return the character value of the given character
            return (int)Encoding.ASCII.GetBytes(ch)[0];
        }

        public static bool validaCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }//é Cnpj

        public static bool validaCpf(string cpf)
        {
            if (cpf == "11111111111" | cpf == "22222222222" | cpf == "33333333333" | cpf == "44444444444" | cpf == "55555555555" | cpf == "66666666666" | cpf == "77777777777" | cpf == "88888888888" | cpf == "99999999999" | cpf == "00000000000")
            {
                return false;
            }

            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }//é cpf

        public static bool validaEmail(string email)//é email
        {
            bool validEmail = false;
            int indexArr = email.IndexOf('@');
            if (indexArr > 0)
            {
                int indexDot = email.IndexOf('.', indexArr);
                if (indexDot - 1 > indexArr)
                {
                    if (indexDot + 1 < email.Length)
                    {
                        string indexDot2 = email.Substring(indexDot + 1, 1);
                        if (indexDot2 != ".")
                        {
                            validEmail = true;
                        }
                    }
                }
            }
            return validEmail;
        }
        public static bool validaCnpjCpf(string Campo)
        {
            Campo = Campo.Replace(".", "").Replace("/", "").Replace(".", "").Replace("\\", "").Replace("-", "");
            if (!validaNumeral(Campo)) return false;
            if ((Campo.Length == 14 && validaCnpj(Campo)) || (Campo.Length == 11 && validaCpf(Campo))) return true;
            return false;
        }

        public static bool validaNumeral(string Campo)
        {
            try
            {
                Int64 result = Int64.Parse(Campo);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }//é numeral


        public static bool validarData(string data)
        {

            DateTime resultado = DateTime.MinValue;
            if (DateTime.TryParse(data, cult, DateTimeStyles.None, out resultado))
                return true;
            return false;

        }

        public static string envioEmail(string Smtp, string From, string Credencial, string Senha, string EmailEnvio, string Titulo, string Body)
        {

            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(From);
                mail.To.Add(EmailEnvio);
                mail.Subject = Titulo;
                mail.Body = Body;
                mail.IsBodyHtml = true;

                using (var smtp = new SmtpClient(Smtp))
                {
                    smtp.EnableSsl = true; // GMail requer SSL
                    smtp.Port = 587;       // porta para SSL
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network; // modo de envio
                    smtp.UseDefaultCredentials = false; // vamos utilizar credencias especificas
                    smtp.Credentials = new NetworkCredential(Credencial, Senha);
                    smtp.Send(mail);

                    return "OK";
                }
            }
            catch (Exception ex)
            {
                return "Erro envio email " + ex;
            }
        }

        public static string CaminhoFisico(string Caminho,int QtdRecuoPasta)
        {
            string[] CaminhoArray = Caminho.Split('\\');
            string NovoCaminho = "";
            for(int i = 0; i < CaminhoArray.Length - 1 - QtdRecuoPasta; i++)
            {
                NovoCaminho = NovoCaminho + CaminhoArray[i] + '\\';
            }
            return NovoCaminho;
        }

        public static string valorVazio(string Valor)
        {
            if(Valor == null)
                return "";
            else
            {
                return Valor;
            }
        }

        public static bool validaNome(string nome)
        {
            nome = nome.Trim();
            var value = Regex.IsMatch(nome, "[A-Za-záàâãéèêëíïóôõöúüçñÁÀÂÃÄÉÈËÍÏÓÔÕÖÚÜÇÑ]");
            var valuenumber = Regex.IsMatch(nome, "[0-9]");
            var valuespecialcaracter = Regex.IsMatch(nome, (@"[!""#$%&'()*+,-./:;?@[\\\]_´{|}~]"));
            if (value && nome.Length >= 3 && !valuenumber && !valuespecialcaracter)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool validaSenha(string senha)
        {
            var number = Regex.IsMatch(senha, "[0-9]");
            var lowerCase = Regex.IsMatch(senha, "[a-z]");
            var upperCase = Regex.IsMatch(senha, "[A-Z]");
            if (number && senha.Length >= 8 && lowerCase && upperCase)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}