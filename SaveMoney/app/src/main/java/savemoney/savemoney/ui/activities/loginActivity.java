package savemoney.savemoney.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import savemoney.savemoney.R;
import savemoney.savemoney.model.Usuario;
import savemoney.savemoney.utils.methodsUtil;

public class loginActivity extends AppCompatActivity {

    @BindView(R.id.editemail)
    EditText email;

    @BindView(R.id.editsenha)
    EditText password;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.buttonlogin)
    public void loginclick() {

        Usuario u = new Usuario();
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        String emailAdress = email.getText().toString();
        String pass = password.getText().toString();




        if (methodsUtil.IsEmail(emailAdress) && emailAdress.equals("teste@teste.com")) {
            {
                dlgAlert.setMessage("Digite a senha corretamente ! ");
                dlgAlert.setTitle("Senha Inválida :");
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

                dlgAlert.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
            }
        } else {

            dlgAlert.setMessage("Digite o email corretamente !");
            dlgAlert.setTitle("Email Inválido :");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            //Toast.makeText(this, "Email ou senha Invalidos !", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.textcadastre)
    public void SingupScreen(){
        Intent i = new Intent(this, signupActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.textesquecisenha)
    public void LostPasswordScreen(){
        Intent i = new Intent(this, recuperarsenhaActivity.class);
        startActivity(i);
    }

}
