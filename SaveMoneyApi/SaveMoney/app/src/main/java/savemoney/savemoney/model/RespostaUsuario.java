package savemoney.savemoney.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RespostaUsuario implements Serializable{

    private Boolean Status;
    private String Mensagem;

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getStrMensagem() {
        return Mensagem;
    }

    public void setStrMensagem(String strMensagem) {
        Mensagem = strMensagem;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    @SerializedName("Json")
    private Usuario usuario;
}
