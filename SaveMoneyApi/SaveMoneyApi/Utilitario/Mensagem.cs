﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace SaveMoneyApi.Utilitario
{
    public class Mensagem
    {
        public bool Status { get; set; }
        public string StrMensagem { get; set; }
        public string Json { get; set; }

        public Mensagem() { }

        public Mensagem(bool p_status, string p_mensagem, string p_json)
        {
            this.Status = p_status;
            this.StrMensagem = p_mensagem;
            this.Json = p_json;
        }

        public HttpResponseMessage HttpResponseMessageGet(HttpStatusCode statuscode, Mensagem objMensagem)
        {

            HttpResponseMessage http = new HttpResponseMessage();
            http.Content = new StringContent(JsonConvert.SerializeObject(objMensagem), Encoding.UTF8, "application/json");
            http.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            http.StatusCode = statuscode;
            return http;

        }

    }
}