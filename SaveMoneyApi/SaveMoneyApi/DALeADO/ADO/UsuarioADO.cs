﻿using Newtonsoft.Json;
using SaveMoneyApi.DALeADO.DAL;
using SaveMoneyApi.Models;
using SaveMoneyApi.Utilitario;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

namespace SaveMoneyApi.DALeADO.ADO
{
    public class UsuarioADO : UsuarioDAL
    {
        private AcessoBD objConn = null;
        Mensagem objMensagem = null;
        public UsuarioADO()
        {

            AcessoBD_s(objConn);
        }

        public UsuarioADO(AcessoBD p_objConn)
        {
            AcessoBD_s(p_objConn);
        }

        private void AcessoBD_s(AcessoBD p_objConn)
        {
            if (p_objConn == null)
            {
                this.objConn = new AcessoBD();
            }
            else
            {
                this.objConn = p_objConn;
            }
        }

        public Mensagem cadastrarUsuario(Usuario objUsuario)
        {

            objMensagem = new Mensagem();

            try
            {
                objConn.AbrirBanco();
                SqlCommand oCmd = new SqlCommand();
                oCmd.CommandText = "insert into Usuario(Nome,Senha,Email,Criado_Em,Modificado_Em,Inativo)OUTPUT INSERTED.[ID] values(@Nome,@Senha,@Email,@Criado_Em,@Modificado_Em,@Inativo)";
                oCmd.Parameters.AddWithValue("@Nome", objUsuario.Nome);
                oCmd.Parameters.AddWithValue("@Senha", objUsuario.Senha);
                oCmd.Parameters.AddWithValue("@Email", objUsuario.Email);
                oCmd.Parameters.AddWithValue("@Criado_Em", objUsuario.Criado_Em);
                oCmd.Parameters.AddWithValue("@Modificado_Em", objUsuario.Modificado_Em);
                oCmd.Parameters.AddWithValue("@Inativo", objUsuario.Inativo);
                using (DataTable odr = objConn.RetornarDataTable(oCmd))
                {
                    //como e insert , nao tem retorno nenhum a principio

                    //objMensagem.StrMensagem = odr.ToString();
                    for (int i = 0; i < odr.Rows.Count; i++)
                    {
                        objUsuario.ID = Int32.Parse(odr.Rows[i]["ID"].ToString());
                    }
                    objMensagem.Status = true;
                    objMensagem.StrMensagem = "Usuario Inserido com sucesso !";
                    //string json = "{ " + '"' + "ID" + '"'+ ":" +  '"' + objUsuario.ID + '"'+ "}";

                }
                objConn.FecharBanco();
            }
            catch (Exception ex)
            {
                objMensagem.Status = false;
                objMensagem.StrMensagem = "Ocorreu uma falha : " + ex.Message + " Rotina :" + string.Format("{0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name) + " line :" + Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                objMensagem.Json = null;
                objConn.RollBackTransaction();
                objConn.FecharBanco();
            }
            return objMensagem;
        }

        public Mensagem editarDadosUsuario(Usuario objUsuario)
        {

            objMensagem = new Mensagem();

            try
            {
                objConn.AbrirBanco();
                SqlCommand oCmd = new SqlCommand();
                oCmd.CommandText = "update Usuario set Nome = @Nome,Senha = @Senha, Email = @Email, Modificado_Em = @Modificado_Em where ID = @ID";
                oCmd.Parameters.AddWithValue("@ID", objUsuario.ID);
                oCmd.Parameters.AddWithValue("@Nome", objUsuario.Nome);
                oCmd.Parameters.AddWithValue("@Senha", objUsuario.Senha);
                oCmd.Parameters.AddWithValue("@Email", objUsuario.Email);
                oCmd.Parameters.AddWithValue("@Modificado_Em", objUsuario.Modificado_Em);
                using (DataTable odr = objConn.RetornarDataTable(oCmd))
                {
                    objMensagem.Status = true;
                    objMensagem.StrMensagem = "Usuario Atualizado com sucesso !";
                    objMensagem.Json = null;
                }
                objConn.FecharBanco();
            }
            catch (Exception ex)
            {
                objMensagem.Status = false;
                objMensagem.StrMensagem = "Ocorreu uma falha : " + ex.Message + " Rotina :" + string.Format("{0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name) + " line :" + Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                objMensagem.Json = null;
                objConn.RollBackTransaction();
                objConn.FecharBanco();
            }
            return objMensagem;
        }

        public Mensagem checarUsuarioExiste(string email)
        {
            objMensagem = new Mensagem();

            try
            {
                objConn.AbrirBanco();
                SqlCommand oCmd = new SqlCommand();
                oCmd.CommandText = "select Nome from Usuario where email = @Email;";
                oCmd.Parameters.AddWithValue("@Email", email);

                using (DataTable odr = objConn.RetornarDataTable(oCmd))
                {
                    if (odr.Rows.Count > 0)
                    {
                        objMensagem.Status = true;
                        objMensagem.StrMensagem = "Usuario já existe";
                        objMensagem.Json = null;
                    }
                    else
                    {
                        objMensagem.Status = false;
                        objMensagem.StrMensagem = "";
                        objMensagem.Json = null;
                    }
                }
                objConn.FecharBanco();
            }
            catch (Exception ex)
            {
                objMensagem.Status = false;
                objMensagem.StrMensagem = "Ocorreu uma falha : " + ex.Message + " Rotina :" + string.Format("{0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name) + " line :" + Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                objMensagem.Json = null;
                objConn.RollBackTransaction();
                objConn.FecharBanco();
            }

            return objMensagem;
        }

        public Mensagem logarUsuario(string email, string senha)
        {

            objMensagem = new Mensagem();

            try
            {
                objConn.AbrirBanco();
                SqlCommand oCmd = new SqlCommand();
                oCmd.CommandText = "select Nome from Usuario where Email = @Email and Senha= @Senha and Inativo = 0 ;";
                oCmd.Parameters.AddWithValue("@Email", email);
                oCmd.Parameters.AddWithValue("@Senha", senha);

                using (DataTable odr = objConn.RetornarDataTable(oCmd))
                {
                    if (odr.Rows.Count > 0)
                    {
                        objMensagem.Status = true;
                        objMensagem.StrMensagem = "Usuario Conectado";
                        objMensagem.Json = null;
                    }
                    else
                    {
                        objMensagem.Status = false;
                        objMensagem.StrMensagem = "Usuário não encontrado";
                        objMensagem.Json = null;
                    }
                }
            }

            catch (Exception ex)
            {
                objMensagem.Status = false;
                objMensagem.StrMensagem = "Ocorreu uma falha : " + ex.Message + " Rotina :" + string.Format("{0}.{1}", MethodBase.GetCurrentMethod().DeclaringType.FullName, MethodBase.GetCurrentMethod().Name) + " line :" + Convert.ToInt32(ex.StackTrace.Substring(ex.StackTrace.LastIndexOf(' ')));
                objMensagem.Json = null;
                objConn.RollBackTransaction();
                objConn.FecharBanco();
            }

            return objMensagem;

        }
    }
}