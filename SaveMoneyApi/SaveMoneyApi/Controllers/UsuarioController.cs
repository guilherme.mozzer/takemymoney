﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using SaveMoneyApi.Utilitario;
using System.Net.Http;
using System.Web.Http;
using SaveMoneyApi.Models;
using SaveMoneyApi.Services;

namespace SaveMoneyApi.Controllers
{
    [RoutePrefix("UsuarioController")]
    public class UsuarioController : ApiController
    {

        Mensagem objMensagem = null;
        UsuarioService service = new UsuarioService();

        [HttpPost]
        [Route("cadastrarUsuario")]
        public HttpResponseMessage cadastrarUsuario(Usuario objUsuario)
        {
            objMensagem = new Mensagem();
            HttpStatusCode codigostatus = HttpStatusCode.OK;
            //atribui a mensagem se encontra null , erro de validação.
            string mensagem = objUsuario == null ? "Preencha todos os dados !"
                           : objUsuario.Nome == null || objUsuario.Nome == "" ? "Preencha o nome !"//if
                           : objUsuario.Email == null || objUsuario.Email == "" ? "Preencha o email"//else if
                           : objUsuario.Senha == null || objUsuario.Senha == "" ? "Preencha a senha !"//else if
                           : Funcao.validaEmail(objUsuario.Email) == false ? "Digite o email corretamente !"//else if
                           : service.checarUsuarioExiste(objUsuario.Email).Status ? "Esse email já foi cadastrado !"//else if
                           : Funcao.validaNome(objUsuario.Nome) == false ? "O nome deve conter no minimo 3 caracteres ,apenas letras maiúsculas e minúsculas"//else if
                           : Funcao.validaSenha(objUsuario.Senha) == false ? "A senha deve conter pelo menos 8 caracteres ,letras maiúsculas, minúsculas e numeros"//else if
                           : "";//else
            //verifica se a mensagem vem vazia!
            if (mensagem != "")
            {
                objMensagem.Json = null;
                objMensagem.Status = false;
                objMensagem.StrMensagem = mensagem;
                codigostatus = HttpStatusCode.NotAcceptable;
            }
            else
            {
                objUsuario.Criado_Em = DateTime.Now;
                objUsuario.Modificado_Em = DateTime.Now;
                objUsuario.Inativo = false;
                objMensagem = service.cadastrarUsuario(objUsuario);//cadastra o usuario
                if (objMensagem.Status)
                    codigostatus = HttpStatusCode.OK;
                else
                    codigostatus = HttpStatusCode.InternalServerError;
            }

            return objMensagem.HttpResponseMessageGet(codigostatus, objMensagem);
        }

        [HttpPost]
        [Route("editarDadosUsuario")]
        public HttpResponseMessage editarDadosUsuario(Usuario objUsuario)
        {
            objMensagem = new Mensagem();
            HttpStatusCode codigostatus = HttpStatusCode.OK;
            string mensagem = objUsuario == null ? "Preencha todos os dados !"
                           : objUsuario.Nome == null || objUsuario.Nome == "" ? "Preencha o nome !"//if
                           : objUsuario.Email == null || objUsuario.Email == "" ? "Preencha o email"//else if
                           : objUsuario.Senha == null || objUsuario.Senha == "" ? "Preencha a senha !"//else if
                           : Funcao.validaEmail(objUsuario.Email) == false ? "Digite o email corretamente !"//else if
                           : service.checarUsuarioExiste(objUsuario.Email).Status ? "Esse email já foi cadastrado !"//else if
                           : Funcao.validaNome(objUsuario.Nome) == false ? "O nome deve conter no minimo 3 caracteres ,apenas letras maiúsculas e minúsculas"//else if
                           : Funcao.validaSenha(objUsuario.Senha) == false ? "A senha deve conter pelo menos 8 caracteres ,letras maiúsculas, minúsculas e numeros"//else if
                           : "";//else
            if (mensagem != "")
            {
                objMensagem.Json = null;
                objMensagem.Status = false;
                objMensagem.StrMensagem = mensagem;
                codigostatus = HttpStatusCode.NotAcceptable;
            }
            else
            {

                //ajustar por todo
                objUsuario.Modificado_Em = DateTime.Now;
                objMensagem = new Mensagem();
                objMensagem = service.editarDadosUsuario(objUsuario);//atualiza os dados do usuario
                if (objMensagem.Status)
                    codigostatus = HttpStatusCode.OK;
                else
                    codigostatus = HttpStatusCode.InternalServerError;

            }
            return objMensagem.HttpResponseMessageGet(codigostatus, objMensagem);
        }

        [HttpPost]
        [Route("loginUsuario")]
        public HttpResponseMessage LogarUsuario(Usuario objUsuario)
        {
            objMensagem = new Mensagem();
            HttpStatusCode codigostatus = HttpStatusCode.OK;

            string mensagem = objUsuario.Email == null ? "Insira o email para logar !"
                           : objUsuario.Senha == null ? "Insira a senha para logar !"
                           : Funcao.validaEmail(objUsuario.Email) == false ? "Digite o email corretamente !"//else if
                           : Funcao.validaSenha(objUsuario.Senha) == false ? "A senha deve conter pelo menos 8 caracteres ,letras maiúsculas, minúsculas e numeros"//else if
                           : "";
            if (mensagem != "")
            {
                objMensagem.Json = null;
                objMensagem.Status = false;
                objMensagem.StrMensagem = mensagem;
                codigostatus = HttpStatusCode.NotAcceptable;
            }
            else
            {
                objMensagem = service.logarUsuario(objUsuario.Email, objUsuario.Senha);
            }

            return objMensagem.HttpResponseMessageGet(codigostatus, objMensagem);
        }
    }
}
