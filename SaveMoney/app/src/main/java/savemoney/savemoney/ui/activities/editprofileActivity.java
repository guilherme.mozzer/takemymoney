package savemoney.savemoney.ui.activities;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import savemoney.savemoney.R;
import savemoney.savemoney.utils.methodsUtil;

public class editprofileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        ButterKnife.bind(this);
    }

    /*@OnClick(R.id.buttoneditperfil)
    public void ClickSaves() {
        Intent i = new Intent(this, loginActivity.class);
        startActivity(i);

    }*/
    @BindView(R.id.editnome)
    EditText nome;

    @BindView(R.id.editemail)
    EditText email;
    @BindView(R.id.confirmemail)
    EditText confemail;

    @BindView(R.id.editsenha)
    EditText editsenha;
    @BindView(R.id.confirmsenha)
    EditText confsenha;

    @OnClick(R.id.buttonsalvarperfil)
    public void clicksalvarperfil() {

        AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

        if (nome.getText().toString().equals("")) {
            dlgAlert.setMessage("Digite seu Nome! ");
            dlgAlert.setTitle("Nome Inválido! ");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else if (!methodsUtil.IsEmail(email.getText().toString())) {
            dlgAlert.setMessage("Digite um E-mail válido! ");
            dlgAlert.setTitle("E-mail inválido! ");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else if (!methodsUtil.IsEmail(confemail.getText().toString())) {
            dlgAlert.setMessage("E-mails Diferentes! ");
            dlgAlert.setTitle("E-mail inválido! ");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else if (!editsenha.getText().toString().equals(confsenha.getText().toString()) || editsenha.getText().toString().equals("")) {
            String msg = !editsenha.getText().toString().equals(confsenha.getText().toString()) ? "As Senhas Diferentes!" : "Digite a Senha!";

            dlgAlert.setMessage(msg);
            dlgAlert.setTitle("Senha inválido: ");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();

            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        } else {
            Intent i = new Intent(this, loginActivity.class);
            startActivity(i);
        }

    }

    @OnClick(R.id.imageback)
    public void ClickBack() {
        Intent i = new Intent(this, menuActivity.class);
        startActivity(i);


    }


}
