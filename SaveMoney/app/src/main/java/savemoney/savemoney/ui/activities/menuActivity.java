package savemoney.savemoney.ui.activities;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import butterknife.OnClick;
import savemoney.savemoney.R;
import savemoney.savemoney.ui.fragments.homepageFragment;
import savemoney.savemoney.ui.fragments.listFragment;
import savemoney.savemoney.ui.fragments.addFragment;
import savemoney.savemoney.ui.fragments.graphicFragment;
import savemoney.savemoney.ui.fragments.configFragment;

import static android.widget.Toast.LENGTH_SHORT;

public class menuActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private ActionBar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        toolbar = getSupportActionBar();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        // load the store fragment by default
        loadFragment(new homepageFragment());
    }

    private boolean loadFragment(android.support.v4.app.Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        android.support.v4.app.Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment = new homepageFragment();
                loadFragment(fragment);
                break;
            case R.id.navigation_list:
                fragment = new listFragment();
                loadFragment(fragment);
                break;
            case R.id.navigation_add:
                fragment = new addFragment();
                loadFragment(fragment);
                break;
            case R.id.navigation_graphic:
                fragment = new graphicFragment();
                loadFragment(fragment);
                break;
            case R.id.navigation_config:
                fragment = new configFragment();
                loadFragment(fragment);
                break;
        }
        return loadFragment(fragment);
    }


}
