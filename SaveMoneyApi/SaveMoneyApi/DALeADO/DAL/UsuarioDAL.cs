﻿using SaveMoneyApi.Models;
using SaveMoneyApi.Utilitario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveMoneyApi.DALeADO.DAL
{
    interface UsuarioDAL
    {
        Mensagem cadastrarUsuario(Usuario objUsuario);

        Mensagem editarDadosUsuario(Usuario objUsuario);

        Mensagem checarUsuarioExiste(string email);

        Mensagem logarUsuario(string email, string senha);
    }
}
