﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveMoneyApi.Models
{
    public class Usuario
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }
        public DateTime Criado_Em { get; set; }
        public DateTime Modificado_Em { get; set; }
        public bool Inativo { get; set; }
    }
}