package savemoney.savemoney.model;

import java.util.Date;

/**
 * Created by 71700609 on 20/11/2018.
 */

public class Usuario {

    private String ID;
    private String Nome;
    private String Email;
    private String Senha;
    private Date Criado_Em;
    private Date Modificado_Em;
    private Boolean Inativo;

    public Boolean getInativo() {
        return Inativo;
    }

    public void setInativo(Boolean inativo) {
        Inativo = inativo;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getSenha() {
        return Senha;
    }

    public void setSenha(String senha) {
        Senha = senha;
    }

    public Date getCriado_Em() {
        return Criado_Em;
    }

    public void setCriado_Em(Date criado_Em) {
        Criado_Em = criado_Em;
    }

    public Date getModificado_Em() {
        return Modificado_Em;
    }

    public void setModificado_Em(Date modificado_Em) {
        Modificado_Em = modificado_Em;
    }
}
