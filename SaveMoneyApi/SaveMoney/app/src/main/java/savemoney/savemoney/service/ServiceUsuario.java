package savemoney.savemoney.service;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import savemoney.savemoney.model.RespostaUsuario;
import savemoney.savemoney.model.Usuario;

public interface ServiceUsuario {

    @POST("UsuarioController/cadastrarUsuario")
    Call<RespostaUsuario> insertCadastro(@Body Usuario u);

    @GET(("UsuarioController/registerUser"))
    Call<Usuario> searchUsuario();

}
