package savemoney.savemoney.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import butterknife.OnClick;
import savemoney.savemoney.R;
import savemoney.savemoney.ui.activities.contaActivity;
import savemoney.savemoney.ui.activities.editprofileActivity;
import savemoney.savemoney.ui.activities.loginActivity;
import savemoney.savemoney.ui.activities.signupActivity;

import static android.widget.Toast.*;


public class configFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view  =  inflater.inflate(R.layout.fragment_config, null);
        Button buttoneditperfil = (Button) view.findViewById(R.id.buttoneditperfil);
        Button buttonsair = (Button) view.findViewById(R.id.buttonsair);
        Button buttoneditconta = (Button) view.findViewById(R.id.buttoneditconta);
        Button buttoneditcategoria = (Button) view.findViewById(R.id.buttoneditcategoria);
        buttoneditperfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                        screenEditProfile();
            }
        });

        buttonsair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                exit();
            }
        });

        buttoneditconta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                screenConta();
            }
        });

        buttoneditcategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

            }
        });

    return view;
    }

    public void screenConta(){
        Intent i = new Intent(getActivity(), contaActivity.class);
        startActivity(i);
    }

    public void screenEditProfile(){
        Intent i = new Intent(getActivity(), editprofileActivity.class);
        startActivity(i);
    }
    public void exit(){
        Intent i = new Intent(getActivity(), loginActivity.class);
        startActivity(i);
    }

}
